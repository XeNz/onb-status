using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StatusChange.Models;

namespace StatusChange
{
    public class StatusHandler : IStatusHandler
    {
        private readonly IActionExecutionResolver _actionExecutionResolver;

        public StatusHandler(IActionExecutionResolver actionExecutionResolver)
        {
            _actionExecutionResolver = actionExecutionResolver;
        }

        /// <summary>
        /// Gets called when getting a submit of a form / forms for example
        /// </summary>
        /// <param name="candidateId"></param>
        /// <param name="phaseId"></param>
        /// <returns></returns>
        public async Task HandleStatusUpdate(int candidateId, int phaseId)
        {
            var repo = new FlowRepository(); // DI this shit

            var phases = repo.GetPhasesForCandidate(candidateId);
            var activePhase = GetCurrentActivePhase(phases, phaseId);

            if (activePhase == null)
                throw new ArgumentNullException(nameof(activePhase));

            var nextPhase = GetNextPhase(phases, activePhase); //#TODO what about the last phase of a flow, how to handle 'nextphase'...
            await _actionExecutionResolver.ExecuteActions(activePhase, nextPhase);
        }

        private static CandidatePhase GetNextPhase(IEnumerable<CandidatePhase> phases, CandidatePhase activePhase)
        {
            var currentOrder = activePhase.Phase.Order;
            var expectedNextOrder = currentOrder++;
            return phases.FirstOrDefault(x => x.Phase.Order == expectedNextOrder && x.Phase.Flow.Id == activePhase.Phase.Flow.Id);
        }


        private static CandidatePhase GetCurrentActivePhase(IEnumerable<CandidatePhase> candidatePhases, int candidatePhaseId)
        {
            return candidatePhases.FirstOrDefault(x => x.Id == candidatePhaseId && x.InternalStatus == InternalStatus.InProgress);
        }
    }

    public class FlowRepository
    {
        public IEnumerable<CandidatePhase> GetPhasesForCandidate(int candidateId)
        {
            // some seed data bleh
            var cand = new Candidate {Id = 1};
            var flows = new List<Flow>
            {
                new Flow("Syteemflow"),
                new Flow("Contractflow")
            };
            var phases = new List<Phase>
            {
                new Phase
                {
                    Id = 1,
                    Flow = flows[0],
                    Name = "Mini-hire prep",
                    Order = 0
                },
                new Phase
                {
                    Id = 1,
                    Flow = flows[0],
                    Name = "Mini-hire",
                    Order = 1
                }
            };

            return new List<CandidatePhase>
            {
                new CandidatePhase
                {
                    Id = 1,
                    Candidate = cand,
                    Phase = phases[0],
                    InternalStatus = InternalStatus.InProgress,
                },
                new CandidatePhase
                {
                    Id = 1,
                    Candidate = cand,
                    Phase = phases[1],
                    InternalStatus = InternalStatus.Inactive,
                },
            };
        }
    }
}