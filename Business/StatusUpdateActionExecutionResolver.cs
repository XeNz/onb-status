using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StatusChange.Models;

namespace StatusChange
{
    public class StatusUpdateActionExecutionResolver : IActionExecutionResolver
    {
        private readonly IEnumerable<IActionHandler> _actionHandlers;

        public StatusUpdateActionExecutionResolver(IEnumerable<IActionHandler> actionHandlers)
        {
            _actionHandlers = actionHandlers;
        }

        public async Task ExecuteActions(CandidatePhase from, CandidatePhase to)
        {
            foreach (var actionHandler in _actionHandlers)
            {
                if(actionHandler.CanHandle(from, to))
                {
                    await actionHandler.Handle(from, to);
                }
            }
        }
    }
}