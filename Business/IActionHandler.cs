using System.Reflection.Metadata;
using System.Threading.Tasks;
using StatusChange.Models;

namespace StatusChange
{
    public interface IActionHandler
    {
        bool CanHandle(CandidatePhase @from, CandidatePhase to);
        Task Handle(CandidatePhase @from, CandidatePhase to);
    }
}