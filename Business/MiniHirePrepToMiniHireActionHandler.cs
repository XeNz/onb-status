using System;
using System.Threading.Tasks;
using StatusChange.Models;

namespace StatusChange
{
    public class MiniHirePrepToMiniHireActionHandler : IActionHandler
    {
        private const string From = "Mini-hire prep";
        private const string To = "Mini-hire";

        public bool CanHandle(CandidatePhase from, CandidatePhase to)
        {
            // maybe use codes for phases so that theres no real string matching... or we enum all of them...
            return string.Equals(from.Phase.Name, From, StringComparison.InvariantCultureIgnoreCase) &&
                   string.Equals(to.Phase.Name, To, StringComparison.InvariantCultureIgnoreCase);
        }

        public async Task Handle(CandidatePhase from, CandidatePhase to)
        {
            UpdateOldPhaseFields(from);
            UpdateOldPhaseStatus(from);
            
            UpdateNewPhaseFields(to);
            UpdateNewPhaseStatus(to);
            
            await ExecuteCustomActions(to, from);

            await UpdateEntitiesInDb(to, from);
        }



        private async Task UpdateEntitiesInDb(CandidatePhase to, CandidatePhase from)
        {
            //map and save both entities bla bla
        }

        private static void UpdateOldPhaseFields(CandidatePhase from)
        {
            from.FinishedAt = DateTime.UtcNow;
        }

        private static void UpdateNewPhaseFields(CandidatePhase to)
        {
            to.AssignedTo = "Team registratie";
            to.AssignedAt = DateTime.UtcNow;
        }

        private static void UpdateOldPhaseStatus(CandidatePhase from)
        {
            from.InternalStatus = InternalStatus.Done;
        }

        private static void UpdateNewPhaseStatus(CandidatePhase to)
        {
            to.InternalStatus = InternalStatus.InProgress;
        }
        
        private Task ExecuteCustomActions(CandidatePhase to, CandidatePhase from)
        {
           // do more custom stuff here
           return Task.CompletedTask;
        }
    }
}