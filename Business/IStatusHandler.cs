using System.Threading.Tasks;

namespace StatusChange
{
    public interface IStatusHandler
    {
        Task HandleStatusUpdate(int candidateId, int phaseId);
    }
}