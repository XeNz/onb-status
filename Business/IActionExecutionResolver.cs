using System.Threading.Tasks;
using StatusChange.Models;

namespace StatusChange
{
    public interface IActionExecutionResolver
    {
        Task ExecuteActions(CandidatePhase from, CandidatePhase to);
    }
}