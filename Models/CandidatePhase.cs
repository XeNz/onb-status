using System;

namespace StatusChange.Models
{
    public class CandidatePhase
    {
        public int Id { get; set; }
        public Phase Phase { get; set; }
        public Candidate Candidate { get; set; }
        public InternalStatus InternalStatus { get; set; }
        
        public DateTime? AssignedAt { get; set; }
        public string AssignedTo { get; set; }
        public DateTime? FinishedAt { get; set; }

    }
}