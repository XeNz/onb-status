namespace StatusChange.Models
{
    public enum InternalStatus
    {
        Inactive,
        InProgress,
        Done
    }
}