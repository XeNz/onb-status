namespace StatusChange.Models
{
    public class Phase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Flow Flow { get; set; }
        public int Order { get; set; }
    }
}