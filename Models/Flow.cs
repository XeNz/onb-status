using System.Collections.Generic;

namespace StatusChange.Models
{
    public class Flow
    {
        public Flow(string name)
        {
            Name = name;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Phase> Phases { get; set; }
    }
}